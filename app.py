import pickle
import numpy as np

from flask import Flask, render_template, request

app = Flask(__name__)
model = pickle.load(open('finalized_model.pkl', 'rb'))

@app.route('/')
def main():
    return render_template('index.html')

@app.route('/predict', methods = ['POST'])
def getPredict():
    Age = request.form['Age']
    employment_type = request.form['employment_type']
    output= request.form['output']
    family_members= request.form['family_members']
    chronic_diseases= request.form['chronic_diseases']
    travel_insurance= request.form['travel_insurance']
    XTest = np.array([[Age, employment_type, output,family_members,chronic_diseases,travel_insurance]], dtype = np.float64)
    
    predicted = model.predict(XTest)[0]
    predicted = 1 / (1 + np.exp(-predicted))
    return render_template('index.html',
    prediction_text = f'Predicted: {predicted * 100:.2f}%')

if __name__ == '__main__':
    app.run(debug = True)